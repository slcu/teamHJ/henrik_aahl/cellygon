"""Top-level package for cellygon."""

from .cellygon import *

__author__ = """Henrik Åhl"""
__email__ = 'henrikaahl@gmail.com'
__version__ = '0.1.5'
