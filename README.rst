========
cellygon
========


.. image:: https://img.shields.io/pypi/v/cellygon.svg
        :target: https://pypi.python.org/pypi/cellygon

.. image:: https://img.shields.io/travis/supersubscript/cellygon.svg
        :target: https://travis-ci.com/supersubscript/cellygon

.. image:: https://readthedocs.org/projects/cellygon/badge/?version=latest
        :target: https://cellygon.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

create cellular meshes from segmented cells


* Free software: GNU General Public License v3
* Documentation: https://cellygon.readthedocs.io.


Features
--------
* Generating polygonised cells (cellygons) from segmented cells
* Only epidermal cells are currently supported

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
